import acp_times
import nose
from nose.tools import *
import arrow
import logging

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def same(s, t):
    """
    Same characters (possibly in different order)
    in two strings s and t.
    """
    return s == t

testing_time = arrow.get("2000-01-01T00:00:00-07:00")

# Testing all the unique open times values
def test_Open0km():
    assert same(str(acp_times.open_time(0, 200, testing_time)), str(testing_time))
    # Time starts at 0:00
def test_Open60km():
    assert same(str(acp_times.open_time(60, 200, testing_time)), str(testing_time.shift(hours=1, minutes=46)))
    # Not really necessary because of the way the french brevet is set up, this is still covered for the 200km version

def test_Open200km():
    assert same(acp_times.open_time(200, 200, testing_time), str(testing_time.shift(hours=5, minutes=53)))


def test_Open400km():
    assert same(acp_times.open_time(400, 400, testing_time), str(testing_time.shift(hours=12, minutes=8)))


def test_Open600km():
    assert same(acp_times.open_time(600, 600, testing_time), str(testing_time.shift(hours=18, minutes=48)))


def test_Open800km():
    assert same(acp_times.open_time(800, 1000, testing_time), str(testing_time.shift(hours=25, minutes=57)))


def test_Open1000km():
    assert same(acp_times.open_time(1000, 1000, testing_time), str(testing_time.shift(hours=33, minutes=5)))

def test_Close0km():
    assert same(acp_times.close_time(0, 200, testing_time), str(testing_time.shift(hours=1)))
    # 0 time ends at an hour in

def test_Close60km():
    assert same(acp_times.close_time(60, 200, testing_time), str(testing_time.shift(hours=4)))
    # this is where the initial bit ends for a french brevet

def test_Close200km():
    assert same(acp_times.close_time(200, 200, testing_time), str(testing_time.shift(hours=13, minutes=20)))


def test_Close400km():
    assert same(acp_times.close_time(400, 400, testing_time), str(testing_time.shift(hours=26, minutes=40)))


def test_Close600km():
    assert same(acp_times.close_time(600, 600, testing_time), str(testing_time.shift(hours=40)))


def test_Close800km():
    assert same(acp_times.close_time(800, 1000, testing_time), str(testing_time.shift(hours=57, minutes=30)))


def test_Close1000km():
    assert same(acp_times.close_time(1000, 1000, testing_time), str(testing_time.shift(hours=75)))

@raises(Exception)
def test_CloseExceptionString():
    acp_times.close_time("a", 200, testing_time)

@raises(Exception)
def test_CloseExceptionList():
    acp_times.close_time([1,2,3,4], 200, testing_time)

@raises(Exception)
def test_OpenExceptionString():
    acp_times.open_time("a", 200, testing_time)

@raises(Exception)
def test_OpenExceptionList():
    acp_times.open_time([1,2,3,4], 200, testing_time)

