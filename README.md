# Project 4: Brevet time calculator with Ajax

Credits to Michal Young & Ram Durairajan for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). Using these values I am implementing the french style of brevet races

## Implementation

This reimplementation is based on the French style of Brevet whereas the first 60km of the brevet is covered at a speed of 20km/h and add an hour on to the closing time, beyond that the rules are the same as an ordinary brevet. This will also give various error messages if the brevet checkpoints are out of bounds in several ways, such as being over the maximum brevet length or being over the length of a truncated brevit. 

In addition, this brevet calculator goes up a little past the maximum distance, merely for sake of an extended finish line of some sort, I.E 1050KM as opposed to 1000KM hard stop, I am unware if brevets actually do this or not, but the website was ambiguous towards the 200KM brevit and it's better safe than sorry. This will go up to 1200KM.

## Testing

I implemented testing similar to the Project 3 tests (parts of which I had directly imported from project 3) and appears to pass the test cases for all the potential out of bounds errors within acp_times.py, everything appears to work fine from my end.

##Notes

I was unable to get the implementation to update when date or brevet length were to change due to my poor understanding of the project and this portion will be completed later on my own time. The code IS updating in the console.log, so I'm assuming there's a minor implementation mishap that I'm just not seeing.

Like prior projects I will omit the credentials.ini from the file and submit it through canvas like before.

Also,  now included is a "run.sh" file which stops any Dockerfiles, Deletes them, and the creates and launches a new one, similar to project 3's included Run.sh file

## Details

Author: Edison Mielke

UOID: edisonm

Email: edisonm@uoregon.edu 
